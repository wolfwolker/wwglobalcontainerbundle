WWGlobalContainerBundle
=======================

This is a simple bundle that provides access to Service Container from everywhere across your project.

Just try \WW\WWGlobalContainerBundle\Model\Sf:: to see autocompetion options provided by your favourite IDE (I recommend PHPStorm :))

Enjoy!
